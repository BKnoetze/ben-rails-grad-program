class Api::V1::BankAccountsController < Api::V1::BaseController
  before_action :find_bank_account, only: [:show, :destroy, :update]

  def index
    bank_accounts = BankAccount.all
    render json: bank_accounts, status: :ok
  end

  def show
    render json: @bank_account, status: :ok
  end

  def create
    new_bank_account = BankAccount.new(bank_account_params)
    if new_bank_account.save
      render json: new_bank_account, status: :ok
    else
      render json: { error: new_bank_account.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    super(@bank_account)
  end

  def update
    if @bank_account.update(bank_account_params)
      render json: @bank_account, status: :ok
    else
      render json: { error: @bank_account.errors }, status: :unprocessable_entity
    end
  end

  private

    def bank_account_params
      params.require(:bank_account).permit(
        :user_id,
        :bank_id,
        :account_number,
        :card_name,
        :expiry_date
      )
    end

    def find_bank_account
      @bank_account = BankAccount.find(params[:id])
    rescue
      render json: { error: "Could not find bank account" }, status: :not_found
    end
end
