class Api::V1::UsersController < Api::V1::BaseController
  before_action :find_user, only: [:show, :destroy, :update]

  def index
    users = User.all
    render json: users, status: :ok
  end

  def show
    render json: @user, status: :ok
  end

  def create
    new_user = User.new(user_params)
    if new_user.save
      render json: new_user, status: :ok
    else
      render json: { error: new_user.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    super(@user)
  end

  def update
    if @user.update(user_params)
      render json: @user, status: :ok
    else
      render json: { error: @user.errors }
    end
  end

  private

    def user_params
      params.require(:user).permit(
        :name,
        :email,
        :last_name,
        :mobile,
        :age
      )
    end

    def find_user
      @user = User.find(params[:id])
    rescue
      render json: { error: "Could not find user" }, status: :not_found
    end
end
