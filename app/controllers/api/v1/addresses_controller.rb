class Api::V1::AddressesController < Api::V1::BaseController
  before_action :find_address, only: [:show, :destroy, :update]

  def index
    addresses = Address.all
    render json: addresses, status: :ok
  end

  def show
    render json: @address, status: :ok
  end

  def create
    new_address = Address.new(address_params)
    if new_address.save
      render json: new_address, status: :ok
    else
      render json: { error: new_address.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def destroy
    super(@address)
  end

  def update
    if @address.update(address_params)
      render json: @address, status: :ok
    else
      render json: { error: @address.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

    def address_params
      params.require(:address).permit(
        :street,
        :user_id
      )
    end

    def find_address
      @address = Address.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: "Could not find address" }, status: :not_found
    end
end
