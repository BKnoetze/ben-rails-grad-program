class Api::BaseController < ApplicationController
  respond_to :json

  def destroy(object)
    ActiveRecord::Base.transaction do
      render json: {}, status: :no_content if object.destroy
    rescue StandardError => error
      render json: { errors: error }, status: :unprocessable_entity
    end
  end
