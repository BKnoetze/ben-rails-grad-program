class Api::V1::AppointmentsController < Api::V1::BaseController
  before_action :find_appointment, only: [:show, :destroy, :update]

  def index
    appointments = Appointment.all
    render json: appointments, status: :ok
  end

  def show
    render json: @appointment, status: :ok
  end

  def create
    new_appointment = Appointment.new(appointment_params)
    if new_appointment.save
      render json: new_appointment, status: :ok
    else
      render json: { error: new_appointment.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    super(@appointment)
  end

  def update
    if @appointment.update(appointment_params)
      render json: @appointment, status: :ok
    else
      render json: { error: @appointment.errors }, status: :unprocessable_entity
    end
  end

  private

    def appointment_params
      params.require(:appointment).permit(
        :date,
        :user_id,
        :doctor_id
      )
    end

    def find_appointment
      @appointment = Appointment.find(params[:id])
    rescue
      render json: { error: "Could not find appointment" }, status: :not_found
    end
end
