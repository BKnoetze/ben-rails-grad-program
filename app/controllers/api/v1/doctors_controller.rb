class Api::V1::DoctorsController < Api::V1::BaseController
  before_action :find_doctor, only: [:show, :destroy, :update]

  def index
    doctors = Doctor.all
    render json: doctors, status: :ok
  end

  def show
    render json: @doctor, status: :ok
  end

  def create
    new_doctor = Doctor.new(doctor_params)
    if new_doctor.save
      render json: new_doctor, status: :ok
    else
      render json: { error: new_doctor.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    super(@doctor)
  end

  def update
    if @doctor.update(doctor_params)
      render json: @doctor, status: :ok
    else
      render json: { error: @doctor.errors }, status: :unprocessable_entity
    end
  end

  private

    def doctor_params
      params.require(:doctor).permit(
        :name,
        :active
      )
    end

    def find_doctor
      @doctor = Doctor.find(params[:id])
    rescue
      render json: { error: "Could not find doctor" }, status: :not_found
    end
end
