class Api::V1::BanksController < Api::V1::BaseController
  before_action :find_bank, only: [:show, :destroy, :update]

  def index
    banks = Bank.all
    render json: banks, status: :ok
  end

  def show
    render json: @bank, status: :ok
  end

  def create
    new_bank = Bank.new(bank_params)
    if new_bank.save
      render json: new_bank, status: :ok
    else
      render json: { error: new_bank.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    super(@bank)
  end

  def update
    if @bank.update(bank_params)
      render json: @bank, status: :ok
    else
      render json: { error: @bank.errors }, status: :unprocessable_entity
    end
  end

  private

    def bank_params
      params.require(:bank).permit(
        :name
      )
    end

    def find_bank
      @bank = Bank.find(params[:id])
    rescue
      render json: { error: "Could not find bank" }, status: :not_found
    end
end
