class Api::V1::PetsController < Api::V1::BaseController
  before_action :find_pet, only: [:show, :destroy, :update]

  def index
    pets = Pet.all
    render json: pets, status: :ok
  end

  def show
    render json: @pet, status: :ok
  end

  def create
    new_pet = Pet.new(pet_params)
    if new_pet.save
      render json: new_pet, status: :ok
    else
      render json: { error: new_pet.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    super(@pet)
  end

  def update
    if @pet.update(pet_params)
      render json: @pet, status: :ok
    else
      render json: { error: @pet.errors }, status: :unprocessable_entity
    end
  end

  private

    def pet_params
      params.require(:pet).permit(
        :name,
        :breed,
      )
    end

    def find_pet
      @pet = Pet.find(params[:id])
    rescue
      render json: { error: "Could not find pet" }, status: :not_found
    end
end
