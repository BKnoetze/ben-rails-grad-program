class BankAccount < ApplicationRecord
  validates :account_number, uniqueness: true, presence: true
  
  belongs_to :bank
  belongs_to :user
end
