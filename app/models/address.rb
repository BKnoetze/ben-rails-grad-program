class Address < ApplicationRecord
  validates :street, presence: true
  
  belongs_to :user
end
