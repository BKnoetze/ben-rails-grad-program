class CreatePetsUsersJoinTable < ActiveRecord::Migration[7.1]
  def change
    create_join_table :pets, :users do |t|
      t.index :pet_id
      t.index :user_id
    end
  end
end
