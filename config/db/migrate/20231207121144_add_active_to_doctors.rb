class AddActiveToDoctors < ActiveRecord::Migration[7.1]
  def change
    add_column :doctors, :active, :boolean
  end
end
