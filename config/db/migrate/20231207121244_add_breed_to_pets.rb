class AddBreedToPets < ActiveRecord::Migration[7.1]
  def change
    add_column :pets, :breed, :string
  end
end
