require 'date'

Bank.destroy_all
Doctor.destroy_all
User.destroy_all
Address.destroy_all
Appointment.destroy_all
BankAccount.destroy_all
Pet.destroy_all

user_1 = User.create({
  name: "Taylor",
  email: "swiftie@swift.com",
  last_name: "Swift",
  mobile: "0825052924",
  age: 22
})

user_2 = User.create({
  name: "Chuckie",
  email: "chuckie@chuck.com",
  last_name: "Chuck",
  mobile: "0826660000",
  age: 6
})

bank_1 = Bank.create({name: "ABSA"})
bank_2 = Bank.create({name: "FNB"})
bank_3 = Bank.create({name: "Scam Bank"})

BankAccount.create(
  {
    account_number: "5538454930817",
    card_name: "Mammon",
    expiry_date: DateTime.new(2016,8,12,23,13,0),
    user_id: user_1.id,
    bank_id: bank_1.id
  }
)

BankAccount.create({
    account_number: "8903788034508",
    card_name: "Rich Beech",
    expiry_date: DateTime.new(2001,1,1,12,05,0),
    user_id: user_2.id,
    bank_id: bank_2.id
})

doctor_1 = Doctor.create({name: "Dr. Phil"})
doctor_2 = Doctor.create({name: "Dr. Quinn, Medicine Woman"})
doctor_3 = Doctor.create({name: "Dr. Quack"})

Appointment.create({
    date: DateTime.new(2023,1,6,1,13,0),
    doctor_id: doctor_1.id,
    user_id: user_2.id
})

Appointment.create({
    date: DateTime.new(2024,5,14,8,9,0),
    doctor_id: doctor_2.id,
    user_id: user_1.id
  })

Address.create({
    street: "Yellow Brick Road",
    user_id: user_1.id
})

Address.create({
    street: "Random Road",
    user_id: user_2.id
})

user_1.pets.create({
  name: "Sophie",
  breed: "Pavement Special",
})

Address.create({
    street: "Yellow Brick Road",
    user_id: user_2.id
})

Address.create({
    street: "Wrong Turn",
    user_id: user_1.id
})
