Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      shallow do
        resources :users
        resources :addresses
        resources :pets
        resources :banks
        resources :bank_accounts, path: 'bank-accounts'
        resources :doctors
        resources :appointments
      end
    end
  end
end
