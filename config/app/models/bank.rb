class Bank < ApplicationRecord

  validates :name, presence: true

  has_many :bank_accounts, dependent: :destroy

end
