class User < ApplicationRecord

  validates :name, presence: true
  validates :email, uniqueness: true, presence: true

  has_one :bank_account, dependent: :destroy
  has_one :bank, through: :bank_account

  has_many :appointments, dependent: :destroy
  has_many :addresses, dependent: :destroy
  has_many :doctors, through: :appointments

  has_and_belongs_to_many :pets

end
